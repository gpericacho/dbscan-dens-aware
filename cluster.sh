#!/bin/bash
echo "running tests"

eps2=(1)
eps=(0.5)
slope=(300)
intercept=(8)

rm -f /home/gonzalop/clustered.txt
/usr/local/hadoop/bin/hadoop fs -rm -r -f /user/gonzalop/output  1> /dev/null
for j in "${!slope[@]}"
do
	for k in "${!intercept[@]}"
	do
		/usr/local/spark/bin/spark-submit --class com.gonzalo.spark.Job3d --master spark://reed:7077 --total-executor-cores 16 --executor-memory 4g dbscan-on-spark-3d-Dens-Aware-0.0.1-SNAPSHOT.jar hdfs://reed:54310/user/gonzalop/partidosTriple.txt hdfs://reed:54310/user/gonzalop/output 700 ${eps[0]} ${eps2[0]} 0.5 ${slope[$j]} ${intercept[k]} 2
	    /usr/local/hadoop/bin/hadoop fs -get /user/gonzalop/output/part-00000 clustered.txt	
	done
done

echo "exiting"
exit

#!/bin/bash

echo "transfering jar"
scp ./target/dbscan-on-spark-3d-Dens-Aware-0.0.1-SNAPSHOT.jar gonzalop@reed.pc.ac.upc.edu:/home/gonzalop

echo "login into cluster"
ssh gonzalop@reed.pc.ac.upc.edu 'bash -s' <cluster.sh

scp -r gonzalop@reed.pc.ac.upc.edu:/home/gonzalop/clustered.txt /Users/gonzalopericacho/DATOS/workspaceScala/dbscan-on-spark-3d-Dens-Aware
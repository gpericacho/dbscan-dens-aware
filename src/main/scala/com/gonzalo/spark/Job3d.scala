package com.gonzalo.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.slf4j.LoggerFactory
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.clustering.dbscan3d.DBSCAN3d
import java.util.Calendar
import org.apache.spark.rdd.RDD
import java.io.FileWriter


object Job3d {


  val log = LoggerFactory.getLogger(Job3d.getClass)

  def main(args: Array[String]) {
    
    if (args.length < 3) {
      System.err.println("You must pass the arguments: <src file> <dest file> <parallelism>")
      System.exit(1)
    }
    
    val (src, dest, maxPointsPerPartition,eps,eps2,uniqueUsers,slope,intercept,partMode) =
      (args(0), args(1), args(2).toInt, args(3).toFloat,args(4).toFloat,args(5).toFloat, args(6).toInt, args(7).toInt,args(8).toInt)
      
    val destOut = dest.split('/').last

    val conf = new SparkConf().setAppName("DBSCAN3D: "+src.split("/").last)
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    val sc = new SparkContext(conf)

    val data = sc.textFile(src,16)  
    
    
    val datainrows= data.map(line => line.split(",",-1).map(elem => elem.trim)) //lines in rows
    val parsedData = datainrows.map(x => (Vectors.dense(Array(x(0),x(1),x(2))
        .map(_.toDouble)),x(3).toLong,x(4),x(5))).cache()
     
    
    val model = DBSCAN3d.train(
      parsedData,
      maxPointsPerPartition = maxPointsPerPartition,
      eps = eps,
      eps2=eps2,
      uniqueUsers=uniqueUsers,
      slope=slope,
      intercept=intercept,
      partMode=partMode)
   
    model.labeledPoints.map(p => s"${p.x},${p.y},${p.z},${p.userId},${p.cluster}").coalesce(1,true).saveAsTextFile(dest)
    
    log.info("Stopping Spark Context...")
    sc.stop()
  }
}
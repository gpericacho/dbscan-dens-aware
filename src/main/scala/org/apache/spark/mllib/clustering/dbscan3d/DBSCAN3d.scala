package org.apache.spark.mllib.clustering.dbscan3d

import org.apache.spark.Logging
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.mllib.clustering.dbscan3d.DBSCAN3dLabeledPoint.Flag
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD
import org.apache.spark.util.SizeEstimator
import java.lang.management.ManagementFactory
import java.io.FileWriter
import java.util.Calendar


object DBSCAN3d {

  /**
   * Train a DBSCAN Model using the given set of parameters
   *
   * @param data training points stored as `RDD[Vector]`
   * only the first two points of the vector are taken into consideration
   * @param eps the maximum distance between two points for them to be considered as part
   * of the same region
   * @param minPoints the minimum number of points required to form a dense region
   * @param maxPointsPerPartition the largest number of points in a single partition
   */
  def train(
    data: RDD[(Vector,Long,String,String)],
    eps: Double,
    maxPointsPerPartition: Int,
    uniqueUsers:Double,
    eps2: Double,
    slope:Int,
    intercept:Int,
    partMode: Int): DBSCAN3d = {

    new DBSCAN3d(eps, maxPointsPerPartition, uniqueUsers,eps2,slope,intercept,partMode, null, null).train(data)

  }

}


class DBSCAN3d private (
  val eps: Double,
  val maxPointsPerPartition: Int,
  val uniqueUsers: Double,
  val eps2: Double,
  val slope:Int,
  val intercept :Int,
  val partMode: Int,
  @transient val partitions: List[(Int,Double,DBSCAN3dCube)],
  @transient private val labeledPartitionedPoints: RDD[(Int, DBSCAN3dLabeledPoint)]
  )extends Serializable with Logging {
  

  def minimumCubeSize = Array(2*eps/111.131,2*eps*360/17662, 2* eps2) //  1° equals to 111.131 km always
                                                                      // 1° equals to 360/17662 km in the polar artic circle
  
  type ClusterId = (Int, Int)        //number of partition and number of cluster
  type Margins = (DBSCAN3dCube, DBSCAN3dCube, DBSCAN3dCube,Double)
  
  def labeledPoints: RDD[DBSCAN3dLabeledPoint] = { labeledPartitionedPoints.values}
 
  private def train(vectors: RDD[(Vector,Long,String,String)]): DBSCAN3d = {
    
     val points= vectors.map(DBSCAN3dPoint).cache() 
     var localPartitions = List[(DBSCAN3dCube, Double)]()
     print("about to find local partitions")
      
     partMode match {
      case 0 =>
        val minimumCubesWithCount =
        vectors
        .map(toMinimumBoundingCube)
        .map((_, 1))
        .aggregateByKey(0)(_ + _, _ + _)
        .collect()
        .toSet  
        
         // find the best partitions for the data space
         localPartitions = PartitionerImprovedWithLevelsFactor.partition(minimumCubesWithCount, maxPointsPerPartition, minimumCubeSize)
        
      case 1 =>
         val minimumCubesWithCount =   //for RDDs
           vectors
          .map(toMinimumBoundingCube)
          .map((_, 1))
          .aggregateByKey(0)(_ + _, _ + _).cache()
         localPartitions = PartitionerRDDCubes.partition(minimumCubesWithCount, maxPointsPerPartition, minimumCubeSize)
     
      case 2 =>
        localPartitions= PartitionerRDD.partition(points, maxPointsPerPartition, minimumCubeSize)
   
     }
 
    print("finished partitioning")
                     

    // grow partitions to include eps
    val localMargins = localPartitions.map({ case (p, density) => (p.shrink(eps), p, p.shrink(-eps),density) }).zipWithIndex 
    val margins = vectors.context.broadcast(localMargins) //The variable is sent to each cluster only once. Is Read-Only variable

    // assign each point to its proper partition
    val groupedPoints = for { point <- points
                          ((inner, main, outer,density), id) <- margins.value 
                          if outer.contains(point)
                         }yield (id, point)
                         
    val numOfPartitions = localPartitions.size
    
    val densityPart = localMargins.map ( p => p._2 -> (getMinPts(p._1._4,slope,intercept),p._1._4)).toMap
    points.unpersist()
    
    // perform local dbscan  
    val clustered = groupedPoints.groupByKey(numOfPartitions).map({case (id,points) => (id,
      new LocalDBSCAN3dNaive(eps,eps2,densityPart(id)._1,uniqueUsers).fit(points))}).flatMapValues(x=>x).cache()
     
     // find all candidate points for merging clusters and group them
    val mergePoints = clustered.flatMap({
          case (partition, point) =>  //for each point
            margins.value.filter({
                case ((inner, main, _,_), _) => main.contains(point) && !inner.almostContains(point) //get partition which has the point between inner & main
              }).map({
                case (_, partitionBelongs) => (partitionBelongs, (partition, point)) //Inserts the point in the corresponding partition
              })
        }).groupByKey()
    
    // find all clusters with aliases from merging candidates
    val adjacencies = mergePoints.flatMapValues(findAdjacencies).values.collect()
    
    
    val adjacencyGraph = adjacencies.foldLeft(DBSCAN3dGraph[ClusterId]()) {
      case (graph, (from, to)) => graph.connect(from, to)
    }

    
    logDebug("About to find all cluster ids")
    // find all cluster ids
    val localClusterIds = clustered.filter({ case (_, point) => point.flag != Flag.Noise })
        .mapValues(_.cluster).distinct().collect().toList

    // assign a global Id to all clusters, where connected clusters get the same id
    val (total, clusterIdToGlobalId) = localClusterIds.foldLeft((0, Map[ClusterId, Int]())) {
      case ((id, map), clusterId) => {
        map.get(clusterId) match {
          case None => { 
            //if cluster not seen before
            val nextId = id + 1
            val connectedClusters = adjacencyGraph.getConnected(clusterId) + clusterId
            val toadd = connectedClusters.map((_, nextId)).toMap //change id of all clusters connected
            (nextId, map ++ toadd)
          }
          case Some(x) =>
            //if cluster seen before discard it
            (id, map)
        }
      }
    }

    val clusterIds = vectors.context.broadcast(clusterIdToGlobalId)
    

    // relabel non-duplicated points
    val labeledInner = clustered.filter(isInnerPoint(_, margins.value)).map {
          case (partition, point) => {
            if (point.flag != Flag.Noise) {
              point.cluster = clusterIds.value((partition, point.cluster))
            }
            (partition, point)
          }
    }
    
    
    // de-duplicate and label merge points
    val labeledOuter= mergePoints.map({case (part,partition) => (part,
          partition.foldLeft(Map[DBSCAN3dLabeledPoint, (DBSCAN3dLabeledPoint,Double)]())({
            case (all, (partition2, point)) =>
             
              if (point.flag != Flag.Noise) {
                point.cluster = clusterIds.value((partition2, point.cluster))
              }
  
              all.get(point) match {
                case None => {
                  if (densityPart(partition2)._2>=densityPart(part)._2){
                    point.minPoints=densityPart(part)._1
                    all + (point -> (point,densityPart(partition2)._2))
                  }else{
                    all
                  }
                }
                case Some((prev,prevDensity)) => {         
                  if (densityPart(partition2)._2>=densityPart(part)._2){
                    point.flag match{
                      case Flag.Core => if (prev.flag!=Flag.Core) {
                            prev.flag=Flag.Core
                            prev.cluster=point.cluster}
                      case Flag.Border => if (prev.flag==Flag.Noise) {
                        prev.flag=Flag.Border
                        prev.cluster=point.cluster
                      }
                      case other =>
                    }
                  }
                  all
                }
              }
          }).values.map(x => x._1)
          )
      }).flatMapValues { x => x }
    
    def time2(calendar: Calendar,timeStamp: Double): Int = {
      calendar.setTimeInMillis(timeStamp.toLong)
      calendar.get(Calendar.DAY_OF_MONTH)*24*60+calendar.getTime.getHours()*60+calendar.getTime.getMinutes()    
    }
    val lpoints=labeledInner.union(labeledOuter).values.cache()
    
    val finalPartitions = localMargins.map {
      case ((_, p, _,density), index) => (index,density ,p)
    }
    
    logDebug("Done") 
    
    new DBSCAN3d(
      eps,
      maxPointsPerPartition,
      uniqueUsers,
      eps2,
      slope,
      intercept,
      partMode,
      finalPartitions,
      labeledInner.union(labeledOuter).cache())
   }
  
  private def toMinimumBoundingCube(vector: (Vector,Long,String,String)): DBSCAN3dCube = {
      val point = DBSCAN3dPoint(vector)
      val x = corner(point.x)
      val y = cornery(point.y)
      val z = cornerz(point.z)
      DBSCAN3dCube(x, y,z, x + minimumCubeSize(0), y + minimumCubeSize(1),z+ minimumCubeSize(2))
  }
  
  private def corner(p: Double): Double =(shiftIfNegative(p) / minimumCubeSize(0)).intValue * minimumCubeSize(0) //moving to the corner of the cubes
  private def cornery(p: Double): Double =(shiftIfNegativey(p) / minimumCubeSize(1)).intValue * minimumCubeSize(1)
  private def cornerz(p: Double): Double =(shiftIfNegativez(p) / minimumCubeSize(2)).intValue * minimumCubeSize(2) //moving to the corner of the cubes
  
  private def shiftIfNegative(p: Double): Double = if (p < 0) p - minimumCubeSize(0) else p
  private def shiftIfNegativey(p: Double): Double = if (p < 0) p - minimumCubeSize(1) else p  
  private def shiftIfNegativez(p: Double): Double = if (p < 0) p - minimumCubeSize(2) else p
  
  
  /**
   * Gets list(clusterid,list of points to be merged) return adjacencies between clusters
   */
  private def findAdjacencies(partition: Iterable[(Int, DBSCAN3dLabeledPoint)]): Set[((Int, Int), (Int, Int))] = {
    
    val zero = (Map[DBSCAN3dLabeledPoint, (Flag.Flag,List[ClusterId])](), Set[(ClusterId, ClusterId)]())

    val (seen, adjacencies) = partition.foldLeft(zero)({

      case ((seen, adjacencies), (partition, point)) =>        
        if (point.flag == Flag.Noise) { // noise points are not relevant for adjacencies
          (seen, adjacencies)
        } else {
          val clusterId = (partition, point.cluster) 
          seen.get(point) match {                    //whether we have already seen this point in other partitions
            case None                => (seen + (point -> (point.flag,List(clusterId))), adjacencies)
            case Some((prevFlag,prevClustersId)) if prevFlag==Flag.Core => (seen, adjacencies + ((prevClustersId.head, clusterId)))
            case Some((prevFlag,prevClustersId)) if (prevFlag==Flag.Border && point.flag==Flag.Core) => (seen + (point -> (point.flag,List(clusterId))), adjacencies ++ prevClustersId.map(prevCluster => (prevCluster, clusterId)).toSet)
            case Some((prevFlag,prevClustersId)) if (prevFlag==Flag.Border && point.flag==Flag.Border) => (seen + (point -> (point.flag,clusterId::prevClustersId)), adjacencies)
          }
        }
    })
    adjacencies
  }
  
  /**
   * Input point and list of growed partitions
   */
  private def isInnerPoint(entry: (Int, DBSCAN3dLabeledPoint),margins: List[(Margins, Int)]): Boolean = {
      entry match {
        case (partition, point) => val ((inner,_,_,_),_) = margins.filter({ case (_, id) => id == partition}).head
                                   inner.almostContains(point)
      }
  }
  
  /**
   * Get MintPts from line equation and density of partition
   */
  private def getMinPts(density:Double,p:Int,c:Int):Int={
    return math.round(density*p+c).toInt
  }
    
}
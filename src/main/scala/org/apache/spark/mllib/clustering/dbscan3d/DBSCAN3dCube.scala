package org.apache.spark.mllib.clustering.dbscan3d

/**
 * A cube with a left lower front corner of (x, y, z) and a right upper back corner of (x2, y2, z2)
 */
case class DBSCAN3dCube(x: Double, y: Double,z: Double,x2: Double, y2: Double,z2: Double) {

  /**
   * Returns whether other is contained by this cube
   */
  def contains(other: DBSCAN3dCube): Boolean = {
    x <= other.x && other.x2 <= x2 && y <= other.y && other.y2 <= y2 && z <= other.z && other.z2 <= z2
  }

  /**
   * Returns whether point is contained by this cube
   */
  def contains(point: DBSCAN3dPoint): Boolean = {
    x <= point.x && point.x <= x2 && y <= point.y && point.y <= y2 && z <= point.z && point.z <= z2
  }

  /**
   * Returns a new cube from shrinking this cube by the given amount from all sides
   */
  def shrink(amount: Double): DBSCAN3dCube = {
    DBSCAN3dCube(x + amount, y + amount,z + amount, x2 - amount, y2 - amount,z2 - amount)
  }

  /**
   * Returns a whether the cube contains the point, and the point
   * is not in the cube's border
   */
  def almostContains(point: DBSCAN3dPoint): Boolean = {
    x < point.x && point.x < x2 && y < point.y && point.y < y2 && z < point.z && point.z < z2
  }

}

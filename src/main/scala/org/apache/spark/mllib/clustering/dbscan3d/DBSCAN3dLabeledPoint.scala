package org.apache.spark.mllib.clustering.dbscan3d

import org.apache.spark.mllib.linalg.Vector

/**
 * Companion constants for labeled points
 */
object DBSCAN3dLabeledPoint {

  val Unknown = 0   //cluster to which belongs, by definition 0 is not belonging to anyone

  object Flag extends Enumeration {
    type Flag = Value
    val Border, Core, Noise, NotFlagged = Value
  }

}

class DBSCAN3dLabeledPoint(vector: (Vector,Long,String,String)) extends DBSCAN3dPoint(vector) {

  def this(point: DBSCAN3dPoint) = this(point.vector)

  var flag = DBSCAN3dLabeledPoint.Flag.NotFlagged
  var cluster = DBSCAN3dLabeledPoint.Unknown
  var visited = false
  var minPoints= -1
  
  override def toString(): String = {
    s"$vector,$cluster,$flag"
  }
  
  override def equals(that: Any) = {
    that match {
      case other: DBSCAN3dLabeledPoint =>super.equals(other)
      case _ => false
    }
  }
}
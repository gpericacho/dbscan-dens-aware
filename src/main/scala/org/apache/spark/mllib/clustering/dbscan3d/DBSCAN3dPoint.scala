package org.apache.spark.mllib.clustering.dbscan3d

import org.apache.spark.mllib.linalg.Vector
import math._

case class DBSCAN3dPoint(val vector: (Vector,Long,String,String)) {
    def x = vector._1(0)
    def y = vector._1(1)
    def z = vector._1(2)/(1000*60*60) //moved to hours
    def userId= vector._2
    def userName= vector._3
    def hashtags= vector._4
    
  def haversieneDist(other: DBSCAN3dPoint): Double = {
      val dLat=(other.x - x).toRadians
      val dLon=(other.y - y).toRadians
 
      val a = pow(sin(dLat/2),2) + pow(sin(dLon/2),2) * cos(x.toRadians) * cos(other.x.toRadians)
      val c = 2 * asin(sqrt(a))
      c * 6372.8  //Earth radius in km 
  }
  
  def distance(other: DBSCAN3dPoint,eps:Double,eps2:Double): Double = {
    val coordDist=haversieneDist(other)
    val dz = other.z - z
    Math.max(coordDist/eps, Math.abs(dz)/eps2)
  }
  
  override def equals(that: Any) = {
    that match {
      case other: DBSCAN3dPoint => other.x==this.x && other.y==this.y && other.z==this.z && other.userId==userId
      case _ => false
    }
  }
}

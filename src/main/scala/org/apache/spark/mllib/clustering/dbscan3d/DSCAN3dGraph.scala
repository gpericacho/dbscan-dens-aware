package org.apache.spark.mllib.clustering.dbscan3d;

import scala.annotation.tailrec

/**
 * Top level method for creating a DBSCANGraph
 */
object DBSCAN3dGraph {

  /**
   * Create an empty graph
   */
  def apply[T](): DBSCAN3dGraph[T] = new DBSCAN3dGraph(Map[T, Set[T]]())

}

/**
 * map key: clusterID value:setClusterIDs
 */
class DBSCAN3dGraph[T] private (nodes: Map[T, Set[T]]) extends Serializable {
  
  
  /**
   * Insert the vertexes and connects in both directions
   *
   */
  def connect(one: T, another: T): DBSCAN3dGraph[T] = {
      this.addEdge(one, another).addEdge(another, one) 
  }
  
  /**
   * Insert an edge from `from` to `to`
   */
  def addEdge(from:T, to:T): DBSCAN3dGraph[T] = {
    nodes.get(from) match {
      case None       => new DBSCAN3dGraph(nodes + (from -> Set(to)))
      case Some(edge) => new DBSCAN3dGraph(nodes + (from -> (edge + to)))
    }
  }
  
  def getDirConnected(from: T): Set[T] = {
    nodes.get(from) match{
    case None       => null
    case Some(adj) => adj
    }
  }
  
  /**
   * Find all vertexes that are reachable from `from`
   */
  def getConnected(from: T): Set[T] = {
    getAdjacent(Set(from), Set[T](), Set[T]()) - from
  }

  @tailrec
  private def getAdjacent(tovisit: Set[T], visited: Set[T], adjacent: Set[T]): Set[T] = {

    tovisit.headOption match {
      case Some(current) =>
        nodes.get(current) match {
          case Some(edges) =>
            getAdjacent(edges.diff(visited) ++ tovisit.tail, visited + current, adjacent ++ edges)
          case None => getAdjacent(tovisit.tail, visited, adjacent)
        }
      case None => adjacent
    }

  }

}

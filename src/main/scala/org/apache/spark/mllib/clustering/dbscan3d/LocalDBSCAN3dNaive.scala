package org.apache.spark.mllib.clustering.dbscan3d

import scala.collection.mutable.Queue

import org.apache.spark.Logging
import org.apache.spark.mllib.clustering.dbscan3d.DBSCAN3dLabeledPoint.Flag
import org.apache.spark.mllib.linalg.Vectors

/**
 * A naive implementation of DBSCAN. It has O(n2) complexity
 * but uses no extra memory. This implementation is not used
 * by the parallel version of DBSCAN.
 *
 */
class LocalDBSCAN3dNaive(eps: Double, eps2:Double, minPoints: Int, uniqueUsers:Double) extends Logging {

  
  def fit(points: Iterable[DBSCAN3dPoint]): Iterable[DBSCAN3dLabeledPoint] = {

    val labeled3dPoints = points.map { new DBSCAN3dLabeledPoint(_) }.toArray //converting points to labeledPoints
    labeled3dPoints.foreach { x => x.minPoints=minPoints }

    val totalClusters =
      labeled3dPoints
        .foldLeft(DBSCAN3dLabeledPoint.Unknown)(
          (cluster, point) => {
            if (!point.visited) {
              point.visited = true

              val neighbors = findNeighbors(point, labeled3dPoints)
              val uniqueNeigbors = numberOfUniqueUsersInNeighborhood(neighbors)
              if (neighbors.size < minPoints || uniqueUsers > uniqueNeigbors.toFloat/minPoints) {
                point.flag = Flag.Noise
                cluster
              } else {
                expandCluster(point, neighbors, labeled3dPoints, cluster + 1)
                cluster + 1
              }
            } else {
              cluster
            }
          })

    logInfo(s"found: $totalClusters clusters")

    labeled3dPoints

  }
  
  private def findNeighbors(point: DBSCAN3dPoint, all: Array[DBSCAN3dLabeledPoint]): Iterable[DBSCAN3dLabeledPoint] =
    all.view.filter(other => { point.distance(other,eps,eps2) <= 1})
    
  private def numberOfUniqueUsersInNeighborhood(neighbors: Iterable[DBSCAN3dLabeledPoint]): Int = {
    val baseCase=(0,Map[Long, Int]())
    
    val (uniqueUsers,seen) = neighbors.foldLeft(baseCase)({
      case ((uniqueUsers, seen),neighbor) =>
        seen.get(neighbor.userId) match {
          case None =>  (uniqueUsers +1 , seen + (neighbor.userId -> 1))
          case Some(_) =>  (uniqueUsers , seen)
        }
    })
    uniqueUsers
  }
  
  private def expandCluster(point: DBSCAN3dLabeledPoint,neighbors: Iterable[DBSCAN3dLabeledPoint],all: Array[DBSCAN3dLabeledPoint],cluster: Int): Unit = {
  
      point.flag = Flag.Core
      point.cluster = cluster
  
      var allNeighbors = Queue(neighbors) //create a queue with all neighbors
  
      while (allNeighbors.nonEmpty) {
        
        allNeighbors.dequeue().foreach(neighbor => { //get first element
          if (!neighbor.visited) {
  
            neighbor.visited = true
            neighbor.cluster = cluster
  
            val neighborNeighbors = findNeighbors(neighbor, all)
  
            if (neighborNeighbors.size >= minPoints && uniqueUsers <= numberOfUniqueUsersInNeighborhood(neighbors).toFloat/minPoints) {
              neighbor.flag = Flag.Core
              allNeighbors.enqueue(neighborNeighbors) //put on them at the end of the queue
            } else {
              neighbor.flag = Flag.Border
            }
          }
          if (neighbor.cluster == DBSCAN3dLabeledPoint.Unknown) {
            neighbor.cluster = cluster
            neighbor.flag = Flag.Border
          }
          
  
        })
  
      }
  
  }
}
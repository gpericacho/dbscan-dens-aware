package org.apache.spark.mllib.clustering.dbscan3d

import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Success, Failure}
import org.apache.spark.Logging
import math._

/**
 * Helper methods for calling the partitioner
 */
object PartitionerImprovedWithLevelsFactor{

  /**
   * Takes minimum cubes with number of points inside and returns all partitions and all points inside each partition
   */
  def partition(minCubes: Set[(DBSCAN3dCube, Int)], maxPointsPerPartition: Long, minimumCubeSize: Array[Double]): List[(DBSCAN3dCube, Double)] = {
    new PartitionerImprovedWithLevelsFactor(maxPointsPerPartition, minimumCubeSize).findPartitions(minCubes)
  }

}



class PartitionerImprovedWithLevelsFactor(maxPointsPerPartition: Long,minimumCubeSize: Array[Double]) extends Logging {
  
  type CubeWithCount = (DBSCAN3dCube, Int)
  type CubeWithDensity = (DBSCAN3dCube, Double)
  
  private def findPartitions(minCubes: Set[CubeWithCount]): List[CubeWithDensity] = {

    val boundingRectangle = findBoundingCube(minCubes)

    val t0 = System.nanoTime()
    
    val partitioned = List[CubeWithCount]()
    val partitions = partition(boundingRectangle, partitioned,minCubes)
   
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) /1000000000+ "s")
    densityPartitions(partitions)
   
  }
  
   private def densityPartitions(partitions: List[CubeWithCount]): List[CubeWithDensity]={
    partitions.map(part => (part._1,part._2/haversieneDist(part._1)*(part._1.y2-part._1.y)))
  }
  
  def haversieneDist(cube: DBSCAN3dCube): Double = {
      var dLat=(cube.x2 - cube.x).toRadians
      var dLon=(cube.y - cube.y).toRadians
      var a = pow(sin(dLat/2),2) + pow(sin(dLon/2),2) * cos(cube.x.toRadians) * cos(cube.x2.toRadians)
      var c = 2 * asin(sqrt(a))
      
      val side1= c * 6372.8  //Earth radius in km 
      
      dLat=(cube.x - cube.x).toRadians
      dLon=(cube.y2 - cube.y).toRadians
      a = pow(sin(dLat/2),2) + pow(sin(dLon/2),2) * cos(cube.x.toRadians) * cos(cube.x2.toRadians)
      c = 2 * asin(sqrt(a))
      
      val side2= c * 6372.8  //Earth radius in km  
      
      side1*side2   //km^2
  }
  
  
  
  private def findBoundingCube(CubesWithCount: Set[CubeWithCount]): DBSCAN3dCube = {
    
    //base case
    val baseCube = DBSCAN3dCube(Double.MaxValue, Double.MaxValue,Double.MaxValue,Double.MinValue, Double.MinValue,Double.MinValue)
    
    //pasa por todos los cubos cogiendo el minimo entre el que tiene y el cubo
    CubesWithCount.foldLeft(baseCube) {
      case (bounding, (cube, _)) =>
        DBSCAN3dCube(bounding.x.min(cube.x), bounding.y.min(cube.y),bounding.z.min(cube.z),bounding.x2.max(cube.x2), bounding.y2.max(cube.y2),bounding.z2.max(cube.z2))
    }

  }
  
  
  /**
   * Gives number of points of a big cube
   */
  private def pointsInCube(space: Set[CubeWithCount], cube: DBSCAN3dCube): Int = {

    space.view.filter({ case (current, _) => cube.contains(current) })
      .foldLeft(0) {
        case (total, (_, count)) => total + count
      }

    }
  
  
  private def partition( boundingRectangle: DBSCAN3dCube, partitioned: List[CubeWithCount],minCubes: Set[CubeWithCount]): List[CubeWithCount] = {
     var partitioned = List[CubeWithCount]() 
     var remaining =  List((boundingRectangle,pointsInCube(minCubes, boundingRectangle)))
     var round=0
     while (remaining.length > 0){
       var toSplit = remaining.filter { x => x._2>maxPointsPerPartition && canBeSplit(x._1) }
       var splited = remaining.filter { x => x._2<=maxPointsPerPartition || !canBeSplit(x._1)}
       partitioned = partitioned ++ splited
       var futures = List[Future[(List[DBSCAN3dCube],Set[(DBSCAN3dCube, Int)])]]()
       
       for((cube,points) <- toSplit){
         val minsquares= minCubes.filter({ case (current, _) => cube.contains(current) })
         val f = Future[(List[DBSCAN3dCube],Set[(DBSCAN3dCube, Int)])] {
            (split(cube),minsquares)
         }
         futures ::= f
       }
       remaining=List[CubeWithCount]() 
       futures.foreach(f => {
             Await.ready(f, Duration.Inf).value.get match {
               case Success((cubes,minCubes)) => remaining = remaining ++ cubes.map { cube => (cube,pointsInCube(minCubes,cube))}
               case Failure(e) => Left(e)
               }
         
       })
       print("round")
       round+=1
     }
     partitioned

  }
  
  /**
   * Returns true if the given rectangle can be split into at least two cubes of minimum size
   */
  private def canBeSplit(box: DBSCAN3dCube): Boolean = {
    (box.x2 - box.x > minimumCubeSize(0) * 2 || box.y2 - box.y > minimumCubeSize(1) * 2 || box.z2- box.z > minimumCubeSize(2) * 2)
  }
  
  /**
   * Splits one cube into 2 with the minimum cost possible
   */
  private def split(box: DBSCAN3dCube): List[DBSCAN3dCube] = {
    
    val midx= (box.x2+box.x)/2
    val midy= (box.y2+box.y)/2
    val midz= (box.z2+box.z)/2
    var splits=List(box)
    var auxList=List[DBSCAN3dCube]()
    
    if (box.x2 - box.x > minimumCubeSize(0) * 2){
      val auxList=List(DBSCAN3dCube(box.x, box.y, box.z, midx, box.y2,box.z2),DBSCAN3dCube(midx, box.y, box.z, box.x2, box.y2,box.z2))
      splits=auxList
    }
    if ( box.y2 - box.y > minimumCubeSize(1) * 2){
      auxList=List[DBSCAN3dCube]()
      for (cube <-splits){
        auxList=DBSCAN3dCube(cube.x, cube.y, cube.z, cube.x2, midy,cube.z2) :: DBSCAN3dCube(cube.x,midy, cube.z, cube.x2, cube.y2,cube.z2) :: auxList
      }
      splits=auxList
    }
    if (box.z2- box.z > minimumCubeSize(2) * 2){
      auxList=List[DBSCAN3dCube]()
      for (cube <-splits){
        auxList=DBSCAN3dCube(cube.x, cube.y, cube.z, cube.x2, cube.y2,midz) :: DBSCAN3dCube(cube.x,cube.y, midz, cube.x2, cube.y2,cube.z2) :: auxList
      }
      splits=auxList    
    }
    splits
  }
}
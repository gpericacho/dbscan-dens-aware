package org.apache.spark.mllib.clustering.dbscan3d

import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import scala.collection.mutable.HashSet
import java.io._
import math._

/**
 * Helper methods for calling the partitioner
 */
object PartitionerRDD{

  /**
   * Takes minimum cubes with number of points inside and returns all partitions and all points inside each partition
   */
  def partition(vectors: RDD[DBSCAN3dPoint] ,maxPointsPerPartition: Long, minimumCubeSize: Array[Double]): List[(DBSCAN3dCube, Double)] = {
    new PartitionerRDD(maxPointsPerPartition, minimumCubeSize).findPartitions(vectors)
  }

}



class PartitionerRDD(maxPointsPerPartition: Long, minimumCubeSize: Array[Double]) extends Serializable {
  
  type CubeWithCount = (DBSCAN3dCube, Long)
  type CubeWithDensity = (DBSCAN3dCube, Double)

  private def findPartitions(points: RDD[DBSCAN3dPoint]): List[CubeWithDensity] = {

    val boundingRectangle = findBoundingCube(points)
    def pointsIn = pointsInCube(points, _: DBSCAN3dCube) //something of type DBSCAN3dCube
    
    val t0 = System.nanoTime()

    val partitions = partitioning((boundingRectangle, pointsIn(boundingRectangle)), pointsIn)
    
    
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) /1000000000+ "s")
    
    densityPartitions(partitions)
   
  }
  
   private def densityPartitions(partitions: List[CubeWithCount]): List[CubeWithDensity]={
    partitions.map(part => (part._1,part._2/haversieneDist(part._1)*(part._1.y2-part._1.y)))
  }
  
  def haversieneDist(cube: DBSCAN3dCube): Double = {
      var dLat=(cube.x2 - cube.x).toRadians
      var dLon=(cube.y - cube.y).toRadians
      var a = pow(sin(dLat/2),2) + pow(sin(dLon/2),2) * cos(cube.x.toRadians) * cos(cube.x2.toRadians)
      var c = 2 * asin(sqrt(a))
      
      val side1= c * 6372.8  //Earth radius in km 
      
      dLat=(cube.x - cube.x).toRadians
      dLon=(cube.y2 - cube.y).toRadians
      a = pow(sin(dLat/2),2) + pow(sin(dLon/2),2) * cos(cube.x.toRadians) * cos(cube.x2.toRadians)
      c = 2 * asin(sqrt(a))
      
      val side2= c * 6372.8  //Earth radius in km  
      
      side1*side2   //km^2
  }
  
  
  private def findBoundingCube(points: RDD[DBSCAN3dPoint]): DBSCAN3dCube = {
    
    val maxx = points.map(x => x.x).max()
    val maxy = points.map(x => x.y).max()
    val maxz = points.map(x => x.z).max()
    val minx = points.map(x => x.x).min()
    val miny = points.map(x => x.y).min()
    val minz = points.map(x => x.z).min() 

    DBSCAN3dCube(minx, miny,minz,maxx,maxy,maxz)
  }
  
  
  /**
   * Gives number of points of a big cube
   */
  private def pointsInCube(points: RDD[DBSCAN3dPoint], cube: DBSCAN3dCube): Long = {
    points.filter(point => cube.contains(point)).count()
  }
  
 
  private def partitioning( boundingRectangle: CubeWithCount, pointsIn: (DBSCAN3dCube) => Long): List[CubeWithCount] = {

     var partitioned = List[CubeWithCount]() 
     var remaining =  List(boundingRectangle)

     while (remaining.length > 0){
       var toSplit = remaining.filter { x => x._2>maxPointsPerPartition && canBeSplit(x._1) }
       var splited = remaining.filter { x => x._2<=maxPointsPerPartition || !canBeSplit(x._1)}
       partitioned = partitioned ++ splited
       remaining=List[CubeWithCount]() 
       for((cube,points) <- toSplit){
           val splited= split(cube,pointsIn)
           remaining= remaining ++ splited
           print("split")
       }
       print("round")
     }
     partitioned
 }
 
 
  
  /**
   * Returns true if the given rectangle can be split into at least two cubes of minimum size
   */
  private def canBeSplit(box: DBSCAN3dCube): Boolean = {
    (box.x2 - box.x > minimumCubeSize(0) * 2 || box.y2 - box.y > minimumCubeSize(1) * 2 || box.z2- box.z > minimumCubeSize(2) * 2)
  }
  
  /**
   * Splits one cube into 2 with the minimum cost possible
   */
  private def split(box: DBSCAN3dCube, pointsIn: (DBSCAN3dCube) => Long):  List[CubeWithCount] = {
    val midx= (box.x2+box.x)/2
    val midy= (box.y2+box.y)/2
    val midz= (box.z2+box.z)/2
    var splits=List(box)
    var auxList=List[DBSCAN3dCube]()
    
    if (box.x2 - box.x > minimumCubeSize(0) * 2){
      val auxList=List(DBSCAN3dCube(box.x, box.y, box.z, midx, box.y2,box.z2),DBSCAN3dCube(midx, box.y, box.z, box.x2, box.y2,box.z2))
      splits=auxList
    }
    if ( box.y2 - box.y > minimumCubeSize(1) * 2){
      auxList=List[DBSCAN3dCube]()
      for (cube <-splits){
        auxList=DBSCAN3dCube(cube.x, cube.y, cube.z, cube.x2, midy,cube.z2) :: DBSCAN3dCube(cube.x,midy, cube.z, cube.x2, cube.y2,cube.z2) :: auxList
      }
      splits=auxList
    }
    if (box.z2- box.z > minimumCubeSize(2) * 2){
      auxList=List[DBSCAN3dCube]()
      for (cube <-splits){
        auxList=DBSCAN3dCube(cube.x, cube.y, cube.z, cube.x2, cube.y2,midz) :: DBSCAN3dCube(cube.x,cube.y, midz, cube.x2, cube.y2,cube.z2) :: auxList
      }
      splits=auxList    
    }
    splits.map { cube => (cube,pointsIn(cube)) }
  }
}